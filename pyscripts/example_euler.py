import numpy as np
import matplotlib.pyplot as plt
from math import cos

# generating t and y array and time-step
N = 30
t = np.linspace(0, 10, N)
h = t[1]-t[0]
y = np.zeros(t.size)

#Initial value
y[0]=1

#function to solve :
def f(t,y) :
   Y = cos(t)
   return(Y)

#Euler's method
for i in np.arange(t.size-1):
  y[i+1] = y[i] + h * f(t[i],y[i])



plt.plot(t, y, c='blue', ls='--', lw=1, alpha=0.5)
plt.show()
