:stem: latexmath
= The Trapezoidal Method

== Introduction

One main drawback of both the Euler method and the backward Euler method is the low convergence order. Next we present a method that has a higher convergence order and in which, at the same time, the stability property is valid for any stepsize stem:[h] in solving the model problem.

== The method

We begin by introducing the trapezoidal rule for numerical integration:

[stem] 
++++ 
\int_a^b g(s)ds \approx \frac{1}{2} (b-a) [g(a) - g(b)]
++++

This rule is illustrated in the next figure. The graph of stem:[y = g(t)] is approximated on interval stem:[ [ a, b \] ] by the linear function stem:[y = p_1(t)] that interpolates stem:[g(t)] at the endpoints of stem:[ [a, b\]]. The integral of stem:[g(t)] over stem:[ [a, b\]] is then approximated by the integral of stem:[p_1(t)] over stem:[ [a, b\]].

image:..//Images/Trapezoidal_method_01.png[image,width=400,height=300]

Since we integrate the differential equation:

[stem] 
++++ 
Y'(t) = f(t,Y(t))
++++

from stem:[t_n] up to stem:[t_{n+1}]: 

[stem] 
++++ 
Y(t_{n+1}) = Y(t_{n}) + \int_{t_{n}}^{t_{n+1}} f(t,Y(t)) dt
++++

Using the trapezoidal rule to approximate the integral, we obtain:

[stem] 
++++ 
y(t_{n+1}) = y(t_{n}) + \frac{h}{2} \left(f(t_n,y_n)+f(t_{n+1},y_{n+1}) \right)
++++

=== Task 1

Solve this problem: 

[stem] 
++++ 
Y'(t) = -10 Y(t) + \frac{1}{1+t^2} +10 \tan^{-1}(t), \quad Y(0)=0
++++

Using Backward Euler's method and the Trapezoidal method for multiple values of time-step :

* stem:[h=0.10]
* stem:[h=0.05]
* stem:[h=0.001]

Compare the solutions obtained with the exact solution stem:[ Y(t) = tan^{-1}(t)]. What can be said about the error? Plot the error as a function of time.

=== Task 2

Determine stability conditions for the method using the same strategy as for Euler's and Backward Euler's Methods.
