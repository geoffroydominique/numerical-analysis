:stem: latexmath
= Elliptic equations

== The equation

A classical example of such PDE is the steady-state heat equation in tow dimensions:

[stem]
++++
\frac{\partial^2 T(x,y)}{\partial x^2} + \frac{\partial^2 T(x,y)}{\partial y^2} = 0
++++

where:

* stem:[T(x,y)] is the temperature of a surface in two dimensions 
* stem:[(x,y)] are the positions on this surface


== Boundary conditions

As for the ODEs, boundary conditions are needed in order to solve the problem. Here we impose these conditions, which are also shown on the next figure:

* stem:[T(0,y) = T_l] is the imposed temperature on the left boundary
* stem:[T(L,r) = T_r] is the imposed temperature on the right boundary
* stem:[T(x,0) = T_b] is the imposed temperature on the bottom boundary
* stem:[T(x,W) = T_b] is the imposed temperature on the top boundary


image:../Images/elliptic_bc.png[image,width=800,height=600]

== The Method

Let's use the definition of the second order derivative which was defined earlier in order to solve the PDE:

[stem]
++++
\frac{\partial^2 T(x,y)}{\partial x^2} \approx \frac{T_{n+1}^j - 2 T_{n}^j + T_{n-1}^j}{(\Delta x)^2}
++++


[stem]
++++
\frac{\partial^2 T(x,y)}{\partial y^2} \approx \frac{T_n^{j+1} - 2 T_{n}^j + T_n^{j-1}}{(\Delta y)^2}
++++

Thus, using such definitions within the PDE:

[stem]
++++
\frac{T_{n+1}^j - 2 T_{n}^j + T_{n-1}^j } {(\Delta x)^2} + \frac{T_{n}^{j+1} - 2 T_{n}^j + T_{n}^{j-1}}{(\Delta y)^2} = 0
++++

One can simplify the equation by using a specific space discretisation : stem:[\Delta x = \Delta y]

[stem]
++++
T_{n+1}^j - 2 T_{n}^j + T_{n-1}^j + T_{n}^{j+1} - 2 T_{n}^j + T_{n}^{j-1} = 0
++++

Or, written differently:

[stem]
++++
4T_i^j = T_{n+1}^j + T_{n-1}^j + T_{n}^{j+1} + T_{n}^{j-1}
++++

== Tasks

=== Mesh

Define a mesh for the problem. What information do you have?



=== Preparing the field

* How many unknowns are there in your problem? 
* How do you plan on scanning on both axis in order to solve the problem.
* How many unknowns do you add to your problem every you solve the equation for a given stem:[n,j]

=== Solving the numerical problem

Solve the problem using the given approximations

